#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 
#include <curl/curl.h>
#include <json-c/json.h>

/* Parse JSON */

const char *parse_json(char *str) {
  struct json_object *jobj;
  struct json_object *access_token;
  jobj = json_tokener_parse(str);

	json_object_object_get_ex(jobj, "access_token", &access_token);

  return json_object_get_string(access_token);
}

struct json_object *parse_room_events(char *str) {
  struct json_object *jobj;
  struct json_object *event_chunk;
  jobj = json_tokener_parse(str);

	json_object_object_get_ex(jobj, "chunk", &event_chunk);

  return event_chunk;
}

/* Process recived body */

struct string {
  char *ptr;
  size_t len;
};

void init_string(struct string *s) {
  s->len = 0;
  s->ptr = malloc(s->len+1);
  if (s->ptr == NULL) {
    fprintf(stderr, "malloc() failed\n");
    exit(EXIT_FAILURE);
  }
  s->ptr[0] = '\0';
}

size_t writefunc(void *ptr, size_t size, size_t nmemb, struct string *s) {
  size_t new_len = s->len + size*nmemb;
  s->ptr = realloc(s->ptr, new_len+1);
  if (s->ptr == NULL) {
    fprintf(stderr, "realloc() failed\n");
    exit(EXIT_FAILURE);
  }
  memcpy(s->ptr+s->len, ptr, size*nmemb);
  s->ptr[new_len] = '\0';
  s->len = new_len;

  return size*nmemb;
}

/* API */

void reduct_messages(struct json_object *events, char* server, char* access_token, char* room_id) {
  int array_len, i;
  array_len = json_object_array_length(events);

  char do_get_events_request_chank1[] = "/_matrix/client/v3/rooms/";
  char do_get_events_request_chank2[] = "/redact/";
  char do_get_events_request_chank3[] = "/";
  char do_get_events_request_chank4[] = "?access_token=";
  char request_body[] = "{\"reason\":\"\"}";
  char event_type_encrypted[] = "m.room.encrypted";
  char event_type_message[] = "m.room.message";

  for (i = 0; i < array_len; i++) {
    struct json_object *event_chunk = json_object_array_get_idx(events, i);
    struct json_object *event_type = json_object_object_get(event_chunk, "type");
    int checkEventTypeEncrypted = strcmp(json_object_get_string(event_type),event_type_encrypted); 
    int checkEventTypeMessage = strcmp(json_object_get_string(event_type),event_type_message); 
    if (checkEventTypeEncrypted == 0 || checkEventTypeMessage == 0) {
      struct json_object *event_id = json_object_object_get(event_chunk, "event_id");
      const char *event_id_str = json_object_get_string(event_id);
      
      char do_reduct_message_request[strlen(server) + sizeof(do_get_events_request_chank1) + strlen(room_id) + sizeof(do_get_events_request_chank2) + strlen(event_id_str) + sizeof(do_get_events_request_chank3) + strlen(event_id_str) + sizeof(do_get_events_request_chank4) + strlen(access_token)];
      strcpy(do_reduct_message_request, server);
      
      strcat(do_reduct_message_request, do_get_events_request_chank1);
      strcat(do_reduct_message_request, room_id);

      strcat(do_reduct_message_request, do_get_events_request_chank2);
      strcat(do_reduct_message_request, event_id_str); 
      
      strcat(do_reduct_message_request, do_get_events_request_chank3);
      strcat(do_reduct_message_request, event_id_str);
      
      strcat(do_reduct_message_request, do_get_events_request_chank4);
      strcat(do_reduct_message_request, access_token); 

      CURL *curl;

      CURLcode res;

      curl = curl_easy_init();

      curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request_body);
      curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_easy_setopt(curl, CURLOPT_URL, do_reduct_message_request);

      res = curl_easy_perform(curl);

      if (res != CURLE_OK) {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
      } else {
        printf("\nRemove %s\n",event_id_str); 
      }
      
      curl_easy_cleanup(curl);
      curl_global_cleanup();
    }
  }
}

struct json_object *get_room_events(char* server, char* access_token, char* room_id) {
  struct json_object *events_result;
  char do_get_events_request_chank1[] = "/_matrix/client/v3/rooms/";
  char do_get_events_request_chank2[] = "/messages?limit=10000&dir=b&access_token=";
  char do_get_events_request[strlen(server) + sizeof(do_get_events_request_chank1) + strlen(room_id) + sizeof(do_get_events_request_chank2) + strlen(access_token)];

  strcpy(do_get_events_request, server);

  printf("%s\n",do_get_events_request);
  strcat(do_get_events_request, do_get_events_request_chank1);
  strcat(do_get_events_request, room_id);

  printf("%s\n",do_get_events_request);
  strcat(do_get_events_request, do_get_events_request_chank2);
  strcat(do_get_events_request, access_token); 
    
  printf("%s\n",do_get_events_request);

  CURL *curl;

  CURLcode res;

  curl = curl_easy_init();

  struct string s;
  init_string(&s);
  curl_easy_setopt(curl, CURLOPT_URL, do_get_events_request);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
  res = curl_easy_perform(curl);

  events_result = parse_room_events(s.ptr); 
  free(s.ptr);
    
  curl_easy_cleanup(curl);

  curl_global_cleanup(); 
  return events_result; 
}

char *get_access_token(char* server, char *login, char *password) {
  char body_chank1[] = "{\"identifier\": {\"type\": \"m.id.user\",\"user\": \"";  
  char body_chank2[] = "\"},\"password\": \"";
  char body_chank3[] = "\",\"type\":\"m.login.password\"}";
  char body[sizeof(body_chank1) + strlen(login) + sizeof(body_chank2) + strlen(password) + sizeof(body_chank3)];

  strcpy(body, body_chank1);
  strcat(body, login);
  free(login);

  strcat(body, body_chank2);
  strcat(body, password);
  free(password);

  strcat(body, body_chank3);
  printf("%s.\n",body);

  char do_login_request[] = "/_matrix/client/v3/login";
  char request_url[strlen(server) + sizeof(do_login_request)];
  strcpy(request_url, server);
  strcat(request_url, do_login_request);
  printf("%s.\n",request_url);

  CURL *curl;
  CURLcode res;
  
  curl = curl_easy_init();

  struct string s;
  init_string(&s);
  curl_easy_setopt(curl, CURLOPT_URL, request_url);
  curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
  res = curl_easy_perform(curl);
 
  char *token_result = strdup(parse_json(s.ptr)); 
  free(s.ptr);

  curl_easy_cleanup(curl);

  curl_global_cleanup();
  return token_result;
}

int main(void)
{ 
  char *login = NULL;
  char *password = NULL;
  char *server = NULL;

  printf("Enter your username: ");
  scanf("%ms", &login);
  password = getpass("Enter your password: ");
  printf("Enter your homeserver (e.g. https://matrix.systemli.org): ");
  scanf("%ms", &server);

  char *token = get_access_token(server, login, password);
  printf("Your token is: %s\n", token);

  char *room_id = NULL;
  printf("Enter the room id: ");
  scanf("%ms", &room_id);

  reduct_messages(get_room_events(server, token, room_id), server, token, room_id);
   
  return 0; 
}