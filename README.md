A simple program in C that removes messages in matrix rooms. 


HOWTO
--------
1. Install dependencies. Execute in terminal `sudo apt-get install libcurl4-openssl-dev` and `sudo apt-get install libjson-c-dev`
2. Download zip from [git repository](https://0xacab.org/czaraunik/matrix_moderation_cli/-/archive/no-masters/matrix_moderation_cli-no-masters.zip)
3. Unpack archive
4. Open directory with unpacked archive in Terminal
5. Execute in terminal `gcc matrix_moderation_cli.c -lcurl -ljson-c -o start`
6. Execute in terminal  `./start`
